TERMINOLOGIE:
Notes: Depending on the context, the variables can either be beliefs; or agents themselves (as in gamma or in the canonical global belief base)

Acquaintances: 
  Type : List[List[Int]]
  Size: (nb_agents, [0,nb_agents-1])
  Ex: pour 3 agents = [[1], [2,0], [1]]
    --> L'agent 0 est influencé par l'agent 1
    --> L'agent 1 est influencé par l'agent 0 et 2
    --> L'agent 2 est influencé par l'agent 1

Revision Policies:
  Type: List[Int]
  Size: nb_agents
  Ex: 3 agents = [1, 1, 1]
    --> Les agents 0,1,2 suivent la même politique de révision à savoir la politique 1. On parle de politique de révision uniforme

Model:
  Type: Tuple(Bool)
    --> à comprendre comme une conjonction de booléen
  Size: nb_variable
  Notation mathématique: M = (v1, ..., vn)
    où vi la valeur booléenne de la variable i
  Ex: 3 variables, (False, True, False)

Formula: 
  Type: Set{Model}
    --> à comprendre comme une disjonction de modèle
  Size: [0, 2^nb_variable]
  Notation Mathématique: F = {M1, ..., Mn}
    où Mi: i-ème modèle de la formule
  Ex: 3 variables d'où 8 modèle max, ici la formule contient 2 modèles
     {(False, True, True), (False, True, False)}

Global Belief State : belief state of every agents
  Type: np.array[Formule]
  Size: nb_agents
  Notation Mathématique: Gb = [F1, ..., Fn]
    où Fi est la formule de l'agent i
  Ex: [ {(False, True, True), (False, True, False)}, # Formule 1
        {(True, False, False), (True, True, True), (True, False, True)}, # Formule 2
        {(False, False, True), (False, False, False),(False, True, True), (False, True, False)} # Formule 3
      ]

Group of Global Belief State (as can be found in algo_1):
  Type: List[Global Belief State]
  Size: 2^(2^nb_variable -1)
  Notation Mathématique: Sgb = [Gbi, ... , Gbn]
    où Gbi: i-ème global belief state sachant les révision policies et le nombre d'agent et leurs relations

Consistency Map (also called gamma):
  Type: Set{Model}
    --> C'est une formule mais dont les variables représentes des agents (ai) et non des connaissances
  Size: [0, 2^nb_agent]
  Notation Mathématique: gamma = {(a1,...,an), ..., (a1, ..., an)}
    où: on a max 2^nb_agent modèle de taille nb_agent
        ai: booléen du i-ème agent
  Ex: 3 agents, gamma = (a1 & ã3) | (ã1 & a2)   avec    ã = non a, & = and, | = or opérateur de la logique propositionelle
                      = {(True,True,False), (True,False,False), (False,True,True), (False,True,False)}


Canonical Global Belief Base (as calculated by get_B_gamma()):
  Type: np.array(Gamma)
  Size: nb_agents
  Notation Mathématique: Cgb = [G1,...,Gn]
    où Gi: formule de type gamma générée à partir d'un gamma précis comme suit 
       Gi = ai & gamma 
  Ex: 3 agents, gamma = (a1 & ã3) | (ã1 & a2) avec ã = non a, & = and, | = or opérateur de la logique propositionelle
      G1 = a1 & ((a1 & ã3) | (ã1 & a2)) = a1 & ã3
         = {(True,True,False), (True,False,False)}
      G2 = a2 & ((a1 & ã3) | (ã1 & a2)) = (a2 & ã1) | (a2 & ã3)
         = {(False,True,True),(False,True,False),(True,True,False),(False,True,False)}
      G3 = a3 & ((a1 & ã3) | (ã1 & a2)) = ã1 & a2 & a3
         = {(False,True,True)}

Consistency Map Interval (as can be found in algo_2):
  Type: Tuple(Consistency Map)
  Size: 2
  Notation Mathématique: I = (G-, G+)
    où: G+ est une Consistency Map correspondant à la borne supérieure
        G- est une Consistency Map correspondant à la borne inférieure
    --> Implicitement cet intervalle est donc plus grand