import numpy as np
from RevisionPolicy import RevisionPolicy
from BRG import BRG
from function import print_models, Exclusive_joint_belief, Consistency_map, print_beliefs, phi_i_ki, generate_gammas, get_B_gamma
from itertools import chain, combinations, product
import time
from algo2 import estep, estep_list, algo1_bis
from algo1_v2 import algo_1_allB

# Exemple de la conférence:
agents = [0, 1, 2]
acquaintances = [[1,2], [0], [0,1]]

beliefs = np.array([
  {(True, False, True), (True, True, False)},
  {(True, True, False), (False, True, False)},
  {(True, False, True), (False, False, True)}
], dtype=object)

R = [4, 6, 1]
revision_policies = np.array([RevisionPolicy.init_revision_policy(i) for i in R])

mybrg = BRG(agents, acquaintances, beliefs, revision_policies)

#Total_it = 1
#beliefs_list = [beliefs]
#print("Initial State:")
#print(beliefs)
#revise_beliefs = mybrg.revision()
#while(not contains(beliefs_list, revise_beliefs)[0]):
#  print_beliefs(revise_beliefs)
#  beliefs_list.append(revise_beliefs)
#  revise_beliefs = mybrg.revision()
#  Total_it += 1
  
#print("\n\nTotal Iterations : ", Total_it)
#print("Cycle size : ", Total_it - contains(beliefs_list, revise_beliefs)[1])

gammaM = {(True, True, False), (False, True, True)}
gammaP = {(True, False, False), (False, True, False), (False, False, True), (True, True, False), (False, True, True), (True, False, True)}
B_M = get_B_gamma(gammaM, 3)
B_P = get_B_gamma(gammaP, 3)

#gammaM = set()
gammaM = {(False, False, False)}
gammaP = set(product(*[[True, False]]*3))
B_M = get_B_gamma(gammaM, 3)
B_P = get_B_gamma(gammaP, 3)

#step1 = estep(agents, acquaintances, revision_policies, B_M, B_P, gammaM, gammaP)
#print(step1)


#print([(gammaM, gammaP)])


#list_B_gammas = algo1_bis(agents, acquaintances, revision_policies)

#print("\nlist_B_gammas : ", list_B_gammas)

#lB=algo_1_allB(agents, acquaintances, revision_policies)
#print(len(lB))
#print("\nlist_B_gammas : ", lB)