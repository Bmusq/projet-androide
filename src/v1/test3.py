import numpy as np
from RevisionPolicy import RevisionPolicy
from BRG import BRG
from function import get_B_gamma, get_Size_Intervalle, get_Size_Intervalles
from itertools import chain, combinations, product
from algo2 import estep, estep_list, algo1_bis

# Exemple de la conférence:
agents = [0, 1, 2]
acquaintances = [[1,2], [0], [0,1]]

beliefs = np.array([
  {(True, False, True), (True, True, False)},
  {(True, True, False), (False, True, False)},
  {(True, False, True), (False, False, True)}
], dtype=object)

R = [4, 6, 1]
revision_policies = np.array([RevisionPolicy.init_revision_policy(i) for i in R])

mybrg = BRG(agents, acquaintances, beliefs, revision_policies)


gammaM = {(True, True, False), (False, True, True)}
gammaP = {(True, False, False), (False, True, False), (False, False, True), (True, True, False), (False, True, True), (True, False, True)}
B_M = get_B_gamma(gammaM, 3)
B_P = get_B_gamma(gammaP, 3)

gammaM = {(False, False, False)}
gammaP = set(product(*[[True, False]]*3))
B_M = get_B_gamma(gammaM, 3)
B_P = get_B_gamma(gammaP, 3)

step0 = [(gammaM, gammaP)]
print(step0)
print("len(I) :", len(step0))
print("Card I :", get_Size_Intervalles(step0))

step1 = estep_list(agents, acquaintances, revision_policies, step0, display=True)
print("step1 : ", step1)
print("len(I) :", len(step1))
print("Card I :", get_Size_Intervalles(step1))

#step2 = estep_list(agents, acquaintances, revision_policies, step1)
#print(step2)
#print("len(I) :", len(step2))
#print("Card I :", get_Size_Intervalles(step2))

#list_gammas, list_B_gamma, maxCycle, maxB = algo1_bis(agents, acquaintances, revision_policies)

#print("\nlist_gammas : ", list_gammas)
#print("list_B_gamma : ", list_B_gamma)
#print("maxCycle : ", maxCycle)
#print("maxB : ", maxB)

