import numpy as np
import time
from BRG import BRG
from RevisionPolicy import RevisionPolicy
from function import generate_gammas, get_B_gamma, step, generate_acquaintances

def algo_1(agents, pa, revision_policies, logs=False, debug=False):
  # GENERATION GAMMA ET REPRESENTANTS
  gammas = generate_gammas(nagent=len(agents))
  B = [get_B_gamma(g,len(agents)) for g in gammas]
  # if debug : print("Nb gamma:",len(B))
  # Verifier qu'on a bien 2^(2^n -1):
  #   3 agents = 128, 4 agents = 32k, 5 agents = 2MM

  # ONE STEP CYCLE REDUCTION
  for acquaintances in pa:
    start = time.time()
    to_keep = set()
    to_throw = set()
    inconsistent = False
    for i in range(len(B)):
      next_b = step(agents, acquaintances, revision_policies, B[i])
      try:
        tmp = np.where(np.array([all(next_b == B_elem) for B_elem in B]))[0][0]
        # if debug : print(i,":",tmp)
        if(not(tmp in to_keep)):
          to_keep.add(tmp)
          if(tmp != i and not(tmp in to_throw)):
            to_throw.add(i)
      except IndexError:
        inconsistent = True
        b_inc = next_b

    ind=[]
    ind += list(to_keep-to_throw)

    # if debug :
    #   print("Keep:",to_keep)
    #   print("Throw:", to_throw)
    #   print("Ind:",ind, len(ind))

    # TAILLE DES CYCLES POUR LES REPRESENTANT RESTANTS
    if(inconsistent):
      B.append(b_inc)
      ind.append(len(B)-1)

    max_gcyc = 0
    for i in ind:
      beliefs = B[i]
      mybrg = BRG(agents, acquaintances, beliefs, revision_policies)
      # if logs: print(beliefs)

      total_it = 1
      bl = [beliefs] # List of belief of the cycle
      rb =  mybrg.revision() # Revised belief

      while(not any([all(rb == bs) for bs in bl])): # bs: belief state
        # if logs: print(revise_beliefs)
        bl.append(rb)
        rb = mybrg.revision()
        total_it += 1

      max_gcyc = max(max_gcyc, total_it - np.where(np.array([all(rb == bs) for bs in bl]))[0][0])

      # if logs:
      #   print("\n\nInd", i)
      #   print("Total Iterations : ", Total_it)
      #   print("Cycle size : ", cyc_size)

    print(acquaintances)
    print("MaxGCyc : ", max_gcyc)
    print("Time:",time.time()-start,"sec\n")
  print("DONE")
  return 0

# PARAMETRE
agents = [0, 1, 2]
pa = generate_acquaintances(agents)
R = [1, 1, 1]
rp = np.array([RevisionPolicy.init_revision_policy(i) for i in R])
_ = algo_1(agents, pa, rp)
