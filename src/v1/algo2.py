from function import print_beliefs,step, generate_acquaintances, get_B_gamma, get_index, generate_gammas, GCons, Adq, ICond, nask_i, Consistency_map, getUnset, ICond_i, get_Size_Intervalle, get_Size_Intervalles, getUnset_both, get_Cycle_size,print_models_2
from RevisionPolicy import RevisionPolicy
import numpy as np
import time
from itertools import chain, combinations, product
import math

def estep(agents, acquaintances, revision_policies, B_M, B_P, gammaM, gammaP, display=False):
  kappa = [[0, False] for i in range(len(agents))]
  Valid = True
  for i in range(len(agents)):
    kappa_i_temp = [len(acquaintances[i]), True]
    
    while not GCons(i, kappa_i_temp[0], gammaM, revision_policies[i], acquaintances[i]):
      kappa_i_temp[0] -= 1

    kappa_i_temp[1] = Adq(i, kappa_i_temp[0], kappa_i_temp[1], gammaM, revision_policies[i], acquaintances[i])
    
    #if(display):
    #  print("kappa_i_temp", kappa_i_temp)
    #  print(i)

    Valid = ICond_i(i, kappa_i_temp[0], kappa_i_temp[1], gammaP, acquaintances[i], revision_policies[i])
    #if not ICond(kappa, gammaP, acquaintances, revision_policies):
    if not Valid:
      #if(display):
        #print("break")
      break

   
    kappa[i] = kappa_i_temp
    nask = nask_i(revision_policies[i], i, acquaintances[i], kappa[i][0], kappa[i][1], len(agents))
    
    B_M[i], B_P[i] = nask & gammaM, nask & gammaP

    #if(display):
    #  print("kappa_i_temp", kappa_i_temp)
    #  print("nask", nask)

  #if(display):
  #  print("kappa " ,kappa)
  #  print("B_M", B_M)
  #  print("B_P", B_P)

  if Valid :
    print("Valid")
    print_models_2("Valid gammaM :", Consistency_map(B_M))
    print_models_2("Valid gammaP :", Consistency_map(B_P))
    return [(Consistency_map(B_M), Consistency_map(B_P))]
  else:
    fX = getUnset(gammaM, gammaP)
    non_fX = set(product(*[[True, False]]*3)) - {fX}
    if(display):
      #print("fX :", fX)
      print_models_2("fX :", {fX})
      #print("non_fX :", non_fX)
      print_models_2("non_fX :", non_fX)
      #print("gammaM :", gammaM)
      print_models_2("gammaM :", gammaM)
      #print("gammaP :", gammaP)
      print_models_2("gammaP :", gammaP)
      #print("gammaM | {fX} :", gammaM | {fX})
      #print("gammaP & {non_fX} :", gammaP & non_fX)
      print_models_2("gammaM | {fX} :", gammaM | {fX})
      print_models_2("gammaP & {non_fX} :",  gammaP & non_fX)

    A = estep(agents, acquaintances, revision_policies, B_M, B_P, gammaM | {fX}, gammaP, display=display)
    B = estep(agents, acquaintances, revision_policies, B_M, B_P, gammaM, gammaP & non_fX, display=display)
    for b in B:
      if not b in A:
        A.append(b)
    return A
    


def estep_list(agents, acquaintances, revision_policies,list_Gammas, display=False):
  list_Gs = []
  for g in list_Gammas:
    Bs = [get_B_gamma(g[0], len(agents)), get_B_gamma(g[1], len(agents))]

    list_Gs += [e for e  in estep(agents, acquaintances, revision_policies, Bs[0], Bs[1], g[0], g[1], display) if not e in list_Gs]
  return list_Gs

def algo1_bis(agents, acquaintances, revision_policies):
  gammaM = {(False, False, False)}
  gammaP = set(product(*[[True, False]]*len(agents)))
  B_M = get_B_gamma(gammaM, len(agents))
  B_P = get_B_gamma(gammaP, len(agents))
  list_gammas = [(gammaM, gammaP)]
  list_B_gamma = []
  print("nb gammas intervalles : ", len(list_gammas))
  print("nb Bs : ", len(list_B_gamma))

  size = get_Size_Intervalle(gammaM, gammaP)
  old_size = size + 1
  # len(size) = 2^n ??

  while old_size > size:

    list_gammas = estep_list(agents, acquaintances, revision_policies,list_gammas)

    print("nb gammas intervalles : ", len(list_gammas))
    #print("list_gammas :", list_gammas)
    #print("Bs : ", list_B_gamma)
    #print("nb Bs : ", len(list_B_gamma))

    old_size = size
    size = get_Size_Intervalles(list_gammas)

  # Calcul maxCycle
  list_B_gamma = [get_B_gamma(g[0], len(agents)) for g in list_gammas]
  maxCycle = 0
  maxB = 0
  for B in list_B_gamma:
    cycleSize = get_Cycle_size(agents, acquaintances, B, revision_policies)
    if cycleSize > maxCycle:
      maxCycle = cycleSize
      maxB = B

  return list_gammas, list_B_gamma, maxCycle, maxB