import numpy as np
from function import print_models, Exclusive_joint_belief, Consistency_map 

agents = [0, 1, 2]
B = np.array([
  {(False, True, True), (True, True, True)}, # A1
  {(True, False, False), (True, True, True), (True, False, True),(True,True,False)}, #A2
  {(True,False,True), (True,False,False)} #A3
], dtype=object)

print_models(Exclusive_joint_belief(agents, B))

print(Consistency_map(B, prints=False))

B_prime = np.array([
  {(False, True, True), (False, True, False),(False,False,True),(False,False,False)}, # A1
  {(True, False, False), (True, False, True), (True, True, True),(True,True,False),(False,True,True),(False,True,False)}, #A2
  {(True,True,True), (True,True,False)} #A3
], dtype=object)

print_models(Exclusive_joint_belief(agents, B_prime))

print(Consistency_map(B_prime, prints=False))