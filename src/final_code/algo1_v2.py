from function import print_beliefs,step, generate_acquaintances, get_B_gamma, get_index, generate_gammas, get_Cycle_size
from RevisionPolicy import RevisionPolicy
import numpy as np
import time
from timeout import TimeoutException, signal

# Algo 1 pour tous les beliefs initiaux possibles.
def algo_1_allB(agents, acquaintances, revision_policies):
  # Initialisation
  gammas = generate_gammas(len(agents), 2)
  all_Bs_from_gammas = []
  for g in gammas:
    beliefs = get_B_gamma(g, len(agents))
    for belief in beliefs:
      if len(belief) > 0:
        all_Bs_from_gammas.append(beliefs)
        break
  B_to_test = list(range(len(all_Bs_from_gammas)))
  B_already_tested = []
  B_selected = []
  ###############

  # tant qu'on a pas testé tous les représentants des gammas
  while(len(B_already_tested) + len(B_selected) < len(gammas)):

    # On prend le prochain représentant, qu'on retire de la pile des représentants non testés.
    current_b = all_Bs_from_gammas[B_to_test[0]]
    id_current_b = B_to_test.pop(0)

    # On fixe le cycle actuel à vide, et on regarde si le représentant a déjà été testé dans les précédents cycles.
    B_current_cycle = []
    in_current_cycle = False
    in_previous_cycle = id_current_b in B_already_tested
    
    # Tant que le représentant n'a pas été testé (n'est ni dans la boucle actuelle, ni dans des boucles précédentes)
    while not in_current_cycle and not in_previous_cycle:
      # On l'ajoute à la boucle actuelle puis on performe un step.
      B_current_cycle.append(id_current_b)
      current_b = step(agents, acquaintances, revision_policies, current_b, display = False)

      # On retire le représentant obtenu avec le step de la liste des représentants non testés.
      try:
        id_current_b = get_index(all_Bs_from_gammas, current_b)
        B_to_test.remove(id_current_b)
      except ValueError:
        pass  # do nothing!

      # On regarde si le représentant a déjà été testé lors de précédentes boucles, puis s'il a déjà été testé dans la boucle actuelle.
      if id_current_b in B_already_tested:
        in_previous_cycle = True
      elif id_current_b in B_current_cycle:
        in_current_cycle = True
      
    # On est sorti de la boucle while, on veut tester pour quelle raison
    if in_previous_cycle: 
      # On a déjà un représentant de ce cycle, on ne le garde pas.
      pass
    elif in_current_cycle: 
      # Premier élément redondant du cycle actuel, on le sélectionne.
      B_selected.append(current_b)
    
    # On ajoute les représentants du cycle actuel que l'on n'a pas sélectionné dans la liste des représentants non séléctionnés.
    B_already_tested.extend(B_current_cycle)
  
  # On retourne la taille du cycle maximum ainsi que la liste des représentants selectionnés (avec la taille de leur cycle)
  return B_selected
      



# Algo 1 pour toutes les acquaintances et tous les beliefs initiaux possibles.      
def algo_1_allA_allB(revision_policies_list, display=False):
  #Initialisation
  agents = range(len(revision_policies_list))
  revision_policies = np.array([RevisionPolicy.init_revision_policy(i) for i in revision_policies_list])
  Possible_acquaintances = generate_acquaintances(agents)
  resultat = []
  maxCycle = 0
  total_time = 0
  #########

  # On test pour toutes les acquaintances possibles
  for i in range(len(Possible_acquaintances)):
    start = time.time()

    # Structure for time out a graph after 10 seconds
    signal.alarm(10)
    try:
      B_reduit_i = algo_1_allB(agents, Possible_acquaintances[i], revision_policies)
    except TimeoutException:
      print("Graph computing time out")
      continue
    else:
      signal.alarm(0)

    end = time.time()
    total_time += end-start
    maxCycle_i=0
    for r in B_reduit_i:
      tmp = max(maxCycle_i, get_Cycle_size(agents, Possible_acquaintances[i], r, revision_policies))
      if(tmp>maxCycle_i):
        maxCycle_i=tmp
        B = r
    resultat.append((maxCycle_i, B_reduit_i))

    if (display):
      print("Time:", end-start, "s")
      print("acquaintances n°", i, Possible_acquaintances[i])
      print("maxCycle : ", maxCycle_i, "   len(B_reduit) = ", len(B_reduit_i))

    if maxCycle < maxCycle_i:
      maxCycle = maxCycle_i
      maxB_reduit = B
      maxAcquaintance = Possible_acquaintances[i]

  
  # Des Stats globales sur toutes les executions
  stats = [0]*(maxCycle)
  for i in range(len(resultat)):
    stats[resultat[i][0]-1]+=1
  print("Pour ", len(revision_policies_list), " agents avec R",revision_policies_list[0],":")
  print("Total Time:", total_time, "s")
  print("Répartition des tailles des maxCycles :\n", list(range(1, len(stats)+1)))
  print(stats)
  print([round(s/len(resultat), 2) for s in stats])
  print("taille du maxCycle :", maxCycle, " (taille max atteinte", stats[-1], "fois)")

  print("\nUne execution max avait comme paramètres :")
  print("Ces Beliefs initiaux : ")
  print_beliefs(maxB_reduit)
  print("Ces Acquaintances :", maxAcquaintance)

  return maxCycle, maxB_reduit, maxAcquaintance


#R = [1, 1, 1]
#maxCycle, maxB_reduit, maxAcquaintance = algo_1_allA_allB(R, display=True)




# Algo 1 pour toutes les politiques de révisions uniformes, toutes les acquaintances et tous les beliefs initiaux possibles.
def algo_1_allR_allA_allB(nb_agents, display=False):
  nb_revision_policies = 6
  for i in range(1, nb_revision_policies+1):
    revision_policies = [i]*nb_agents

    print("\nPour", nb_agents, "agents avec des politiques de révisions uniformes", i,":")

    maxCycle, maxB_reduit, maxAcquaintance = algo_1_allA_allB(revision_policies, display)

#nb_agents = 3
#algo_1_allR_allA_allB(nb_agents)



# reduction des gammas != cacul de tailles de cycles
# donc reduction des cycles puis appliquer get_Cycle_size