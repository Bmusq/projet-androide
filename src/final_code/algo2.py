from function import print_beliefs, generate_acquaintances, get_B_gamma, GCons, Adq, nask_i, Consistency_map, getUnset, ICond_i, get_Size_Intervalle, get_Size_Intervalles, get_Cycle_size,print_models_2
from RevisionPolicy import RevisionPolicy
import numpy as np
import time
from timeout import TimeoutException, signal
from itertools import chain, combinations, product
import math

def estep(agents, acquaintances, revision_policies, B_M, B_P, gammaM, gammaP, display=False):
  kappa = [[0, False] for i in range(len(agents))]
  Valid = True
  for i in range(len(agents)):
    kappa_i_temp = [len(acquaintances[i]), True]
    
    while not GCons(i, kappa_i_temp[0], gammaM, revision_policies[i], acquaintances[i]):
      kappa_i_temp[0] -= 1

    kappa_i_temp[1] = Adq(i, kappa_i_temp[0], kappa_i_temp[1], gammaM, revision_policies[i], acquaintances[i])
    
    #if(display):
    #  print("kappa_i_temp", kappa_i_temp)
    #  print(i)

    Valid = ICond_i(i, kappa_i_temp[0], kappa_i_temp[1], gammaP, acquaintances[i], revision_policies[i])
    #if not ICond(kappa, gammaP, acquaintances, revision_policies):
    if not Valid:
      #if(display):
        #print("break")
      break

   
    kappa[i] = kappa_i_temp
    nask = nask_i(revision_policies[i], i, acquaintances[i], kappa[i][0], kappa[i][1], len(agents))
    
    B_M[i], B_P[i] = nask & gammaM, nask & gammaP

    #if(display):
    #  print("kappa_i_temp", kappa_i_temp)
    #  print("nask", nask)

  #if(display):
  #  print("kappa " ,kappa)
  #  print("B_M", B_M)
  #  print("B_P", B_P)

  if Valid :
    if(display):
      print("Valid")
      print_models_2("Valid gammaM :", Consistency_map(B_M))
      print_models_2("Valid gammaP :", Consistency_map(B_P))
    return [(Consistency_map(B_M), Consistency_map(B_P))]
  else:
    fX = getUnset(gammaM, gammaP)
    non_fX = set(product(*[[True, False]]*3)) - {fX}
    #print("fX : ", fX)
    if(display):
      #print("fX :", fX)
      print_models_2("fX :", {fX})
      #print("non_fX :", non_fX)
      print_models_2("non_fX :", non_fX)
      #print("gammaM :", gammaM)
      print_models_2("gammaM :", gammaM)
      #print("gammaP :", gammaP)
      print_models_2("gammaP :", gammaP)
      #print("gammaM | {fX} :", gammaM | {fX})
      #print("gammaP & {non_fX} :", gammaP & non_fX)
      print_models_2("gammaM | {fX} :", gammaM | {fX})
      print_models_2("gammaP & {non_fX} :",  gammaP & non_fX)

    # changer BM et BP ?
    A = estep(agents, acquaintances, revision_policies, B_M, B_P, gammaM | {fX}, gammaP, display=display)
    B = estep(agents, acquaintances, revision_policies, B_M, B_P, gammaM, gammaP & non_fX, display=display)
    for b in B:
      if not b in A:
        A.append(b)
    return A
    


def estep_list(agents, acquaintances, revision_policies,list_Gammas, display=False):
  list_Gs = []
  for g in list_Gammas:
    Bs = [get_B_gamma(g[0], len(agents)), get_B_gamma(g[1], len(agents))]

    list_Gs += [e for e  in estep(agents, acquaintances, revision_policies, Bs[0], Bs[1], g[0], g[1], display) if not e in list_Gs]
    print(g[0])
  return list_Gs

def algo1_bis(agents, acquaintances, revision_policies):
  gammaM = {(False, False, False)}
  gammaP = set(product(*[[True, False]]*len(agents)))
  B_M = get_B_gamma(gammaM, len(agents))
  B_P = get_B_gamma(gammaP, len(agents))
  list_gammas = [(gammaM, gammaP)]
  list_B_gamma = []

  size = get_Size_Intervalle(gammaM, gammaP)
  old_size = size + 1

  while old_size > size:

    list_gammas = estep_list(agents, acquaintances, revision_policies,list_gammas)

    old_size = size
    size = get_Size_Intervalles(list_gammas)

    print("nb gammas intervalles : ", len(list_gammas))
    print("Card intervalles = ", size)

  # Calcul maxCycle
  list_B_gamma = [get_B_gamma(g[0], len(agents)) for g in list_gammas]
  maxCycle = 0
  maxB = 0
  #for B in list_B_gamma:
  #  cycleSize = get_Cycle_size(agents, acquaintances, B, revision_policies)
  #  if cycleSize > maxCycle:
  #    maxCycle = cycleSize
  #    maxB = B

  return list_gammas, list_B_gamma, maxCycle, maxB


# Algo 1 pour toutes les acquaintances et tous les beliefs initiaux possibles.      
def algo_2_allA_allB(revision_policies_list, display=False):
  #Initialisation
  agents = range(len(revision_policies_list))
  revision_policies = np.array([RevisionPolicy.init_revision_policy(i) for i in revision_policies_list])
  Possible_acquaintances = generate_acquaintances(agents)
  resultat = []
  maxCycle = 0
  total_time = 0
  maxB_reduit = np.array([set(), set(), set()], dtype=object)
  maxAcquaintance = None
  #########

  # On test pour toutes les acquaintances possibles
  for i in range(len(Possible_acquaintances)):
    start = time.time()

    # Structure for time out a graph after 10 seconds
    signal.alarm(10)
    try:
      list_gammas, list_B_gamma, _, _ = algo1_bis(agents, Possible_acquaintances[i], revision_policies)
    except TimeoutException:
      print("Graph computing time out")
      continue
    else:
      signal.alarm(0)

    end = time.time()
    total_time += end-start
    maxCycle_i=0
    for r in list_B_gamma:
      tmp = max(maxCycle_i, get_Cycle_size(agents, Possible_acquaintances[i], r, revision_policies))
      if(tmp>maxCycle_i):
        maxCycle_i=tmp
        B = r
    resultat.append((maxCycle_i, list_B_gamma))

    if (display):
      print("Time:", end-start, "s")
      print("acquaintances n°", i, Possible_acquaintances[i])
      print("maxCycle : ", maxCycle_i, "   len(B_reduit) = ", len(list_B_gamma))

    if maxCycle < maxCycle_i:
      maxCycle = maxCycle_i
      maxB_reduit = B
      maxAcquaintance = Possible_acquaintances[i]

  
  # Des Stats globales sur toutes les executions
  stats = [0]*(maxCycle+1)
  for i in range(len(resultat)):
    stats[resultat[i][0]-1]+=1
  print("Pour ", len(revision_policies_list), " agents avec R",revision_policies_list[0],":")
  print("Total Time:", total_time, "s")
  print("Répartition des tailles des maxCycles :\n", list(range(1, len(stats)+1)))
  print(stats)
  print([round(s/len(resultat), 2) for s in stats])
  print("taille du maxCycle :", maxCycle, " (taille max atteinte", stats[-1], "fois)")

  print("\nUne execution max avait comme paramètres :")
  print("Ces Beliefs initiaux : ")
  print_beliefs(maxB_reduit)
  print("Ces Acquaintances :", maxAcquaintance)

  return maxCycle, maxB_reduit, maxAcquaintance


#R = [1, 1, 1]
#maxCycle, maxB_reduit, maxAcquaintance = algo_1_allA_allB(R, display=True)




# Algo 1 pour toutes les politiques de révisions uniformes, toutes les acquaintances et tous les beliefs initiaux possibles.
def algo_2_allR_allA_allB(nb_agents, display=False):
  nb_revision_policies = 6
  for i in range(1, nb_revision_policies+1):
    revision_policies = [i]*nb_agents

    print("\nPour", nb_agents, "agents avec des politiques de révisions uniformes", i,":")

    maxCycle, maxB_reduit, maxAcquaintance = algo_2_allA_allB(revision_policies, display)