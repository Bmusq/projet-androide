# import random
import numpy as np
from BRG import BRG
from RevisionPolicy import RevisionPolicy 
from function import get_index
# Exemple de la conférence:
agents = [0, 1, 2]
acquaintances = [[1], [2,0], [1]]

beliefs = np.array([
  {(False, True, True), (False, True, False)}, # Aaron
  {(True, False, False), (True, True, True), (True, False, True)}, # Betty
  {(False, False, True), (False, False, False),(False, True, True), (False, True, False)} # Cheryl
], dtype=object)

R = [4, 6, 1]
revision_policies = np.array([RevisionPolicy.init_revision_policy(i) for i in R])

mybrg = BRG(agents, acquaintances, beliefs, revision_policies)

Total_it = 1
beliefs_list = [beliefs]
print("Initial State:")
print(beliefs,'\n')
revise_beliefs = mybrg.revision()
while(get_index(beliefs_list, revise_beliefs) == -1):
  print(revise_beliefs,'\n')
  beliefs_list.append(revise_beliefs)
  revise_beliefs = mybrg.revision()
  Total_it += 1

print(revise_beliefs)  
  
print("\n\nTotal Iterations : ", Total_it)
print("Cycle size : ", Total_it - get_index(beliefs_list, revise_beliefs)) 