import numpy as np

class BRG:
  def __init__(self, agents, acquaintances, beliefs, revision_policies):
    # List of agent's IDs
    self.V = agents
    # List of agent's acquaintances : (i,j) in A => j is aware of i's beliefs
    self.A = acquaintances
    # List of agent's initial beliefs
    self.B = beliefs
    # List of agent's revision policies
    self.R = revision_policies
    self.step = 1

  def toString(self):
    print(self.V, self.A, self.B, self.R)

  def revision(self):
    new_B = []
    #print("\nRevision:", self.step)
    for agent in self.V:
      acquaintances = self.A[agent]
      if len(acquaintances) != 0:
        new_B.append(self.R[agent].revision(self.B[self.A[agent]], self.B[agent]))
      else: 
        new_B.append(self.R[agent].revision([], self.B[agent]))
      #print("revision agent : ", agent)
      #print(new_B[-1])
    self.B = np.array(new_B)
    self.step +=1
    return self.B

