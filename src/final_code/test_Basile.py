
#*************************************************************************
#********************************  LORE  *********************************
#*************************************************************************
# def equivalent(beliefs, revise_beliefs):
#   if(len(beliefs) != len(revise_beliefs)):
#     return False
#   for b,rb in zip(beliefs,revise_beliefs):
#     if(len(b) != len(rb)):
#       return False
#     for t_b, t_rb in zip(b,rb):
#       if(t_b != t_rb):
#         return False
#   return True
  
# def contains(beliefs_list, revise_beliefs):
#   for belief_id in range(len(beliefs_list)):
#     if(equivalent(beliefs_list[belief_id], revise_beliefs)):
#       return True, belief_id
#   return False, 0
# 
# def getElemLen(g):
#   for e in g: break
#   return len(e)
# 
# DEPRECATED : reduction des representants équivalents
#  -----> Aucunes équivalence (expected)
# list_index = [[""] for i in range(len(B))]
# equiv = np.zeros(len(B))
# for i in range(len(B)):
#   for j in range(len(B)):
#     if(equivalent(B[i], B[j])):
#       equiv[i] += 1
#       list_index[i].append(j)
# print(equiv)
# list_index = list(np.unique([list_index[i][1:] for i in range(len(B))]))
# B = [B[list_index[j][0]] for j in range(len(list_index))]
# print(len(B))

# def cartesian_product(*arrays):
#     la = len(arrays)
#     dtype = np.result_type(*arrays)
#     arr = np.empty([len(a) for a in arrays] + [la], dtype=dtype)
#     for i, a in enumerate(np.ix_(*arrays)):
#         arr[...,i] = a
#     return arr.reshape(-1, la)

# def generate_gammas(nvar=3, nagent=3, nvalue=2):
#   if nvalue == 2:
#     domain = [True, False]
#   else:
#     domain = np.linpsace(0,nvalue-1,nvalue, dtype=int)
#   gamma = list(product(*[domain]*nvar))[:-1]
#   gammas = list(product(*[gamma]*nagent))[:-1]
#   return gammas

# def cartesian_product_transpose(*arrays):
#     broadcastable = numpy.ix_(*arrays)
#     broadcasted = numpy.broadcast_arrays(*broadcastable)
#     rows, cols = numpy.prod(broadcasted[0].shape), len(broadcasted)
#     dtype = numpy.result_type(*arrays)
#     out = numpy.empty(rows * cols, dtype=dtype)
#     start, end = 0, rows
#     for a in broadcasted:
#         out[start:end] = a.reshape(-1)
#         start, end = end, end + rows
#     return out.reshape(cols, rows).T

# timer = datetime.now()
# gamma = list(product(*[[True,False]]*3))
# print(datetime.now()-timer)

# domain= [numpy.arange(2)]
# timer = datetime.now()
# gamma = cartesian_product_transpose(*(domain * 3))
# print(print(datetime.now()-timer))

# x= True
# y = False
# print((not x) & y)

# a = {2,3,4}
# b = {4,5,6}
# c = True
# print(a & c)
# print(a | c)

# # (True, True) & (False, True)

# beliefs = np.array([
#   {(True, True, True), (False, True, True)}, # a1
#   {(True, True, True), (True, False, True), (True, True, False), (True, False, False)}, # a2
#   {(True, False, True), (True, False, False)} # a3
# ])
# a = beliefs[0]
# b = beliefs[1]
# c = beliefs[2]
# print(a & b)
# print(a & c)



# def generate_random_BRG(nbAgents, nbVariables):
  
#     V = list(range(nbAgents))
    
#     A = []
#     for i in range(nbAgents):
#       j = random.randrange(nbAgents)
#       A.append(list(set(random.choices(V, k=j)))) #Pour supprimer les doublons
#     print(A)

#     # B est une liste de set de liste de booléen 
#     # un set pour chaque agent
#     # une liste pour chaque modèle :
#     # exemple set modèles : a et c = {[1,0,1], [1,1,1]}
#     B = []
#     mini = 0
#     maxi = nbVariables
#     for i in range(nbAgents):
#       nb_v_def = random.randrange(mini, maxi)
#       v_def = random.sample(list(range(nbVariables)), nb_v_def)
#       s = set()
#       l = np.zeros(nbVariables)
#       l[v_def] = random.sample(list(range(2)), nb_v_def)
#       for i in l:
#         s.add()

#     B = np.array(B)
#     print(B)

    # R = np.array([init_revision_policy(random.randrange(1,7)) for i in range(nbAgents)])

    # return BRG(V, A, B, R)

# # print(belief[0])
# nvalue=2
# nvar=3
# nagent=3
# B_all = []
# if nvalue == 2:
#   domain = [True, False]
# else:
#   domain = np.linpsace(0,nvalue-1,nvalue, dtype=int)
# B_all = list(product(*[domain]*nvar))

# ss = np.array(list(chain(*map(lambda x: combinations(B_all, x), range(0, len(B_all)+1)))), dtype=object)[1:]

# # beliefs = np.array([
# #   {(True, True, True), (False, True, True)}, # a1
# #   {(True, True, True), (True, False, True), (True, True, False), (True, False, False)}, # a2
# #   {(True, False, True), (True, False, False)} # a3
# # ])
# B = []
# gamma_class = set()
# for si in ss:
#   B.append(set(si))
#   for sj in ss:
#     B.append(set(sj))
#     for sk in ss:
#       B.append(set(sk))
#       gamma_class.add(tuple(Consistency_map(B, prints=False)))
#       B.pop(2)
#     B.pop(1)
#     print(".",end='',flush=True)
#   B.pop(0)
#   print("+",flush=True)
# print(len(gamma_class)  )