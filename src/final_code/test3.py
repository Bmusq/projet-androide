import numpy as np
from RevisionPolicy import RevisionPolicy
from BRG import BRG
from function import get_B_gamma, get_Size_Intervalle, get_Size_Intervalles, get_Cycle_size, print_beliefs
from itertools import chain, combinations, product
from algo2 import estep, estep_list, algo1_bis
from algo1_v2 import algo_1_allB

# Exemple de la conférence:
agents = [0, 1, 2]
acquaintances = [[1,2], [0,2], [0, 1]]

beliefs = np.array([
  {(True, False, True), (True, True, False)},
  {(True, True, False), (False, True, False)},
  {(True, False, True), (False, False, True)}
], dtype=object)

# R3 et R6 aboutissent à une erreur "Stop Iteration"
R = [2, 2, 2]
revision_policies = np.array([RevisionPolicy.init_revision_policy(i) for i in R])

mybrg = BRG(agents, acquaintances, beliefs, revision_policies)

gammaM = {(True, True, False), (False, True, True)}
gammaP = {(True, False, False), (False, True, False), (False, False, True), (True, True, False), (False, True, True), (True, False, True)}
B_M = get_B_gamma(gammaM, len(agents))
B_P = get_B_gamma(gammaP, len(agents))

gammaM = {tuple([False]*len(agents))}
gammaP = set(product(*[[True, False]]*len(agents)))
B_M = get_B_gamma(gammaM, len(agents))
B_P = get_B_gamma(gammaP, len(agents))

step0 = [(gammaM, gammaP)]
print(step0)
print("len(I) :", len(step0))
print("Card I :", get_Size_Intervalles(step0))

#step1 = estep_list(agents, acquaintances, revision_policies, step0, display=False)
list_gammas, list_B_gamma, maxCycle, maxB = algo1_bis(agents, acquaintances, revision_policies)
#print("step1 : ", step1)
print("len(I) :", len(list_gammas))
print("Card I :", get_Size_Intervalles(list_gammas))
print("maxCycle = ", maxCycle)
print("maxB = ", maxB)


max_cycle_size = 0
for interval in list_gammas:
  b = get_B_gamma(interval[0], len(agents))
  print_beliefs(b)
  maxi = get_Cycle_size(agents, acquaintances, b, revision_policies)
  if max_cycle_size < maxi:
    max_cycle_size = maxi
    max_b = b
print("\nMaxCycleSize algo 2 = ", max_cycle_size)


max_cycle_size = 0
res = algo_1_allB(agents, acquaintances, revision_policies)
for r in res:
  max_cycle_size = max(max_cycle_size, get_Cycle_size(agents, acquaintances, r, revision_policies))
print("\nmaxCycleSize algo 1 =", max_cycle_size)

#step2 = estep_list(agents, acquaintances, revision_policies, step1)
#print(step2)
#print("len(I) :", len(step2))
#print("Card I :", get_Size_Intervalles(step2))

#list_gammas, list_B_gamma, maxCycle, maxB = algo1_bis(agents, acquaintances, revision_policies)

#print("\nlist_gammas : ", list_gammas)
#print("list_B_gamma : ", list_B_gamma)
#print("maxCycle : ", maxCycle)
#print("maxB : ", maxB)





# Note : 
# pour ces paramètres :
# agents = [0, 1, 2]
# acquaintances = [[1,2], [0,2], [0,1]]
# R = [2, 2, 2]
# On obtient un MaxCycleSize de 1 pour l'algo 2

# Algo 1 sileBa:
# agents = [0, 1, 2]
# acquaintances = [[1,2], [0,2], [0,1]]
# MaxGCyc = 2 pour ces beliefs issues de gamma:
# beliefs = [{100} {010} {001}] , [{101}{011}{101,011}], [{110}{110,011}{011}]
#            [{010,110}{110}{101}]

