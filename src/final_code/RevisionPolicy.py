from itertools import combinations

class RevisionPolicy:
  def init_revision_policy(ID):
    if (ID == 1):
      return R1()
    if (ID == 2):
      return R2()
    if (ID == 3):
      return R3()
    if (ID == 4):
      return R4()
    if (ID == 5):
      return R5()
    if (ID == 6):
      return R6()

  def phi_max(self, Ci, Bi, mu=True):
    stop = False
    max_k = 0
    phi_list = [] # List phi_k
    for k in range(1, len(Ci)+1): # Iteration sur les k
        if (stop): # si aucune combinaison pour le L précédent ne sont consistentes, on stop
          break
        else :
          stop = True
          # On set le stop à True en attendant une combinaison consistente

        for K in combinations(Ci, k):
            # K = liste de k sets de modèles d'agents
            # -> une combinaison possible de L agents parmi les voisins
            phi_temp = mu
            if mu == True:
              phi_temp = K[0]

            # Calcul Conjonction de Kj: formules -> set
            for Kj in K:
              phi_temp = phi_temp & Kj # Intersection d'ensemble
            
            # Calcul Disjonction
            if len(phi_temp) > 0:
              # this combination is consistent : a model exists
              if max_k < k:
                # a bigger combination, reset phi_max and set stop at False
                max_k = k
                phi_list.append(set(phi_temp))
                stop = False
              else :
                # not the first combination of this size, add it to phi_max
                # La disjonction dans le papier
                phi_list[-1] = phi_list[-1] | phi_temp
    return phi_list

class R1(RevisionPolicy):

  def get_id(self):
    return 1
  
  def revision(self, Ci, Bi):
    phi_list = self.phi_max(Ci, Bi, True)
    if len(phi_list) != 0 :
      phi_max = phi_list[-1]
      return phi_max
    else :
      return set()

class R2(RevisionPolicy):

  def get_id(self):
    return 2
  
  def revision(self, Ci, Bi):
    phi_list = self.phi_max(Ci, Bi, True)
    if len(phi_list) != 0 :
      phi_max = phi_list[-1]
      phi_max_and_Bi = phi_max & Bi
      if len(phi_max_and_Bi) > 0: # is consistent (max = max_Bi)
        return phi_max_and_Bi
      else : # isn't consistent (max > max_Bi)
        return phi_max
    else :
      return Bi

class R3(RevisionPolicy):

  def get_id(self):
    return 3
  
  def revision(self, Ci, Bi):
    phi_list = self.phi_max(Ci, Bi, True)
    if len(phi_list) != 0 :
      phi_max = phi_list[-1]
      phi_max_and_Bi = phi_max & Bi
      if len(phi_max_and_Bi) > 0: # is consistent (max = max_Bi)
        return phi_max_and_Bi
      else : # isn't consistent (max > max_Bi)
        if len(phi_list) > 1: # si max_phi > 1 (donc si phi_max-1 existe)
          return phi_max | (phi_list[-2] & Bi)
        else :
          return phi_max | Bi
    else :
      return Bi

class R4(RevisionPolicy):

  def get_id(self):
    return 4
  
  def revision(self, Ci, Bi):
    phi_list = self.phi_max(Ci, Bi, True)
    if len(phi_list) != 0 :
      phi_max = phi_list[-1]
      phi_max_and_Bi = phi_max & Bi
      if len(phi_max_and_Bi) > 0: # is consistent (max = max_Bi)
        return phi_max_and_Bi
      else : # isn't consistent (max > max_Bi)
        # return phi_max_and_Bi
        return phi_max | Bi
    else :
      return Bi

class R5(RevisionPolicy):

  def get_id(self):
    return 5
  
  def revision(self, Ci, Bi):
    phi_list = self.phi_max(Ci, Bi, True)
    if len(phi_list) != 0 :
      phi_max = phi_list[-1]
      phi_max_and_Bi = phi_max & Bi
      if len(phi_max_and_Bi) > 0: # is consistent (max = max_Bi)
        return phi_max_and_Bi
      else : # isn't consistent (max > max_Bi)
        # return Bi
        return Bi
    else :
      return Bi

class R6(RevisionPolicy):

  def get_id(self):
    return 6
  
  def revision(self, Ci, Bi):
    phi_list = self.phi_max(Ci, Bi, Bi)
    if len(phi_list) != 0 :
      phi_max = phi_list[-1]
      phi_max_and_Bi = phi_max & Bi
      return phi_max_and_Bi
    else :
      return Bi