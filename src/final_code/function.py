from itertools import chain, combinations, product
import numpy as np
from BRG import BRG
import math

def generate_belief(nvar, nagent, nvalue=2):
  B_all = []
  if nvalue == 2:
    domain = [True, False]
  else:
    domain = np.linpsace(0,nvalue-1,nvalue, dtype=int)
  B_all = list(product(*[domain]*nvar))

  subset = np.array(list(chain(*map(lambda x: combinations(B_all, x), range(0, len(B_all)+1)))), dtype=object)[1:]

  return subset

def generate_gammas(nagent=3, nvalue=2):
  res = []
  if nvalue == 2:
    domain = [True, False]
  else:
    domain = np.linpsace(0,nvalue-1,nvalue, dtype=int)
  gamma = list(product(*[domain]*nagent))
  gamma.pop(-1)
  gammas = np.array(list(chain(*map(lambda x: combinations(gamma, x), range(0, len(gamma)+1)))), dtype=object)[1:]
  for g in gammas: # Overhead
    res.append(set(g))
  return res

def get_B_gamma(gamma,nb_a):
  B_gamma = np.array([set() for i in range(nb_a)], dtype=object)
  if len(gamma) > 0:
    for g in gamma:
      for i in range(len(g)):
        if g[i]:
          B_gamma[i].add(g)
    return B_gamma
  else:
    return B_gamma
  
def step(agents, acquaintances, revision_policies, B, display=False):
  BRG_S = BRG(agents, acquaintances, B, revision_policies)
  B_1 = BRG_S.revision()
  gamma_B_1 = Consistency_map(B_1)
  B_1_gamma = get_B_gamma(gamma_B_1,len(agents))
  if display: 
    print("B:",B)
    print("NextB:",B_1)
    print("Gamma:",gamma_B_1)
    print("B_gamma:",B_1_gamma)
  return B_1_gamma

def print_beliefs(beliefs):
  for Bi in beliefs:
    print_models(Bi)
  print()

def print_models(models):
  if len(models) == 0:
    string = "empty_set"
  else : 
    string = ""
    string += "{"
    for model in models:
      for boolean in model:
        if boolean:
          string += "1"
        else:
          string += "0"
      string += "  "
    string = string[:-2]
    string += "}"
  print(string)

def print_models_2(name, models):
  if len(models) == 0:
    string = "empty_set"
  else : 
    string = ""
    string += "{"
    for model in models:
      for boolean in model:
        if boolean:
          string += "1"
        else:
          string += "0"
      string += "  "
    string = string[:-2]
    string += "}"
  print(name + string)

def non_beliefs_opti(B, agent_array, nb_agents=3):
  #selector = [x for x in range(len(B)) if not x in agent_array]
  selector = list(set(range(len(B))) - set(agent_array))
  non_belief_set = B[selector]
  return non_belief_set

def Exclusive_joint_belief(agent_array, B, prints=False):
  if prints: 
    print("Tb for agents ", np.array(agent_array)+1, " : ")
    print(agent_array)

  if len(agent_array) == 0:
    return set()
  belief_set = B[agent_array]
  #
  #non_belief_set = selector(B, agent_array)
  #
  non_belief_set = np.delete(B, agent_array)
  #
  Tb = belief_set[0]
  for Bi in belief_set:
    Tb = Tb & Bi
  for non_Bi in non_belief_set:
    Tb = Tb - non_Bi
  if prints:
    print("B:")
    print_beliefs(B)
    print("Bx:")
    print_beliefs(belief_set)
    print("Bv/x:")
    print_beliefs(non_belief_set)
    print("Tb : ")
    print_models(Tb)
    print()
  return Tb

def Consistency_map(B, prints=False):
  gamma_B = set()
  agents = range(len(B))
  set_of_Xs = np.array(list(chain(*map(lambda x: combinations(agents, x), range(0, len(agents)+1)))), dtype=object)[1:]
  for X in set_of_Xs:
    Tbx = Exclusive_joint_belief(np.array(X), B, prints)
    if len(Tbx) != 0:
      fx = np.full(len(B), False)
      fx[np.array(X)] = True
      gamma_B.add(tuple(fx))
  if prints:
    print("for B = ")
    print_beliefs(B)
    print("gamma_B = ")
    print_models(gamma_B)
    print("\n")
  return(gamma_B)

def generate_acquaintances(agents):
  domains = [[i for i in chain(range(m), range(m + 1, len(agents)))] for m in agents]

  acquaintances = []
  for dom in domains:
    temp = []
    for i in range(0, len(dom)+1):
      temp += list(map(list, combinations(dom, i)))
    acquaintances.append(temp)

  Possible_acquaintances = list(map(list, list(product(*acquaintances))))

  return Possible_acquaintances

def get_index(GBS_set, GBS):
  for i in range(len(GBS_set)):
    if all(GBS_set[i] == GBS):
      return i
  return -1

## GCons
def GCons(i, k_i, gamma, alpha_i, Ai):
  if k_i == 0:
    return True
  else :
    g_and_p = gamma_and_phi(i, gamma, Ai, k_i)
    if (alpha_i.get_id() < 6) :
      if len(g_and_p) > 0: # Consistency (not consequence of false)
        return True
    else:
      ## Calcul des modèles n'ayant pas un ai = False
      g_and_p_and_a = gamma_and_phi_and_ai(i, g_and_p)
      if len(g_and_p_and_a) > 0: # Consistency (not consequence of false)
        return True
  return False

# CCond 
def CCond(k, gamma, A, alpha):
  for i in range(len(A)):
    if not (GCons(i, k[i][0], gamma, alpha[i], A[i]) and PAdq(i, k[i][1], k[i][0], gamma, alpha[i], A[i])):
      return False
  return True

# ICond 
def ICond(k, gamma, A, alpha):
  for i in range(len(A)):
    #print("GCONS ", i, " ", GCons(i, k[i][0] + 1, gamma, alpha[i], A[i]) )
    if not (not GCons(i, k[i][0] + 1, gamma, alpha[i], A[i]) and NAdq(i, k[i][1], k[i][0], gamma, alpha[i], A[i])):
      return False
  return True

# ICond_i
def ICond_i(i, k_i, delta_i, gamma, A_i, alpha_i):
  return not GCons(i, k_i + 1, gamma, alpha_i, A_i) and NAdq(i, delta_i, k_i, gamma, alpha_i, A_i)

# is_k_max
def is_k_max(k, gamma, A, alpha):
  return CCond(k, gamma, A, alpha) and ICond(k, gamma, A, alpha)

### Adq : Codé "top level" -> proche de l'écriture mathématique, il y a surement une approche plus efficace

def Adq(i, k_i, delta_i, gamma, alpha_i, A_i):
  #print("Padq ",PAdq(i, delta_i, k_i, gamma, alpha_i, A_i))
  #print("NAdq ",NAdq(i, delta_i, k_i, gamma, alpha_i, A_i))
  return PAdq(i, delta_i, k_i, gamma, alpha_i, A_i) and NAdq(i, delta_i, k_i, gamma, alpha_i, A_i)

def PAdq(i, delta_i, k_i, gamma, alpha_i, A_i):
  if not delta_i:
    return True
  else :
    if alpha_i.get_id() == 1:
      return True
    g_and_p = gamma_and_phi(i, gamma, A_i, k_i)
    return len(gamma_and_phi_and_ai(i,g_and_p)) > 0

def NAdq(i, delta_i, k_i, gamma, alpha_i, A_i):
  if delta_i:
    return True
  else :
    if alpha_i.get_id() == 1:
      return False
    g_and_p = gamma_and_phi(i, gamma, A_i, k_i)
    return len(gamma_and_phi_and_ai(i,g_and_p)) == 0


### gamma_and_phi : Codé "top level" -> proche de l'écriture mathématique, il y a surement une approche plus efficace
def phi_i_ki(i, A_i, k_i, nagent, nvalue=2):
  res = set()
  ais = set([ai for ai in list(product(*[[True, False]]*nagent))])
  for c in combinations(A_i, k_i):
    for ai in ais:
      boolean = True
      for i in c:
        if ai[i] == False:
          boolean = False
          break
      if boolean:
        res.add(tuple(ai))
  return res

def gamma_and_phi(i, gamma, A_i, k_i):
  phi = phi_i_ki(i, A_i, k_i, len(next(iter(gamma))))
  gamma_and_phi = phi & gamma
  return gamma_and_phi

def gamma_and_phi_and_ai(i, gamma_and_phi):
  gamma_and_phi_and_ai = set()
  for model in gamma_and_phi:
    if model[i] == True:
      gamma_and_phi_and_ai.add(model)
  return gamma_and_phi_and_ai

# nask
def nask_i(Ri, i, A_i, k_i, delta_i, nb_agents):
  if Ri.get_id() == 1:
    return nask1(i, A_i, k_i, nb_agents)
  if Ri.get_id() == 2:
    return nask2(i, A_i, k_i, delta_i, nb_agents)
  if Ri.get_id() == 3:
    return nask3(i, A_i, k_i, delta_i, nb_agents)
  if Ri.get_id() == 4:
    return nask4(i, A_i, k_i, delta_i, nb_agents)
  if Ri.get_id() == 5:
    return nask5(i, A_i, k_i, delta_i, nb_agents)
  if Ri.get_id() == 6:
    return nask6(i, A_i, k_i, delta_i, nb_agents)
  print("problem nask")

def nask1(i, A_i, k_i, nb_agents):
  phi = phi_i_ki(i, A_i, k_i, nb_agents)
  return phi

def nask2(i, A_i, k_i, delta_i, nb_agents):
  phi = phi_i_ki(i, A_i, k_i, nb_agents)
  ais = set([ai for ai in list(product(*[[True, False]]*nb_agents)) if ai[i] == True])
  if delta_i:
    return phi & ais
  else :
    return phi | ais

def nask3(i, A_i, k_i, delta_i, nb_agents):
  phi_ki = phi_i_ki(i, A_i, k_i, nb_agents)
  phi_ki_and_ai = set()
  for model in phi_ki:
    if model[i] == True:
      phi_ki_and_ai.add(model)

  if delta_i or k_i == 0:
    return phi_ki_and_ai
  else :
    phi_ki_minus_1 = phi_i_ki(i, A_i, k_i - 1, nb_agents)
    phi_ki_minus_1_and_ai = set()
    for model in phi_ki_minus_1:
      if model[i] == True:
        phi_ki_minus_1_and_ai.add(model)
    return phi_ki_and_ai | (phi_ki | phi_ki_minus_1_and_ai)
  
def nask4(i, A_i, k_i, delta_i, nb_agents):
  phi_ki = phi_i_ki(i, A_i, k_i, nb_agents)
  phi_ki_and_ai = set()
  for model in phi_ki:
    if model[i] == True:
      phi_ki_and_ai.add(model)

  if delta_i:
    return phi_ki_and_ai
  else :
    ais = set([ai for ai in list(product(*[[True, False]]*nb_agents)) if ai[i] == True])
    #print(phi_ki)
    #print(ais)
    return phi_ki | ais

def nask5(i, A_i, k_i, delta_i, nb_agents):
  phi_ki = phi_i_ki(i, A_i, k_i, nb_agents)
  ais = set([ai for ai in list(product(*[[True, False]]*nb_agents)) if ai[i] == True])
  if delta_i :
    return phi_ki & ais
  else :
    return phi_ki | ais

def nask6(i, A_i, k_i, delta_i, nb_agents):
  phi_ki = phi_i_ki(i, A_i, k_i, nb_agents)
  phi_ki_and_ai = set()
  for model in phi_ki:
    if model[i] == True:
      phi_ki_and_ai.add(model)
  return phi_ki_and_ai


# GetUnset 
def getUnset_invGammaP(gammaM, gammaP):
  Xs = list(product(*( [True, False]*3)))
  for X in Xs:
    if not X in gammaM:
      if not X in gammaP:
        return X

def getUnset(gammaM, gammaP):
  # Avec cette fonction, on ne peut renvoyer que des X de taille 1, donc des X = {1} ou X = {2} et pas de X = {1,2} v {1} par exemple
  if len(gammaP) == 1:
    print("\nfX == gammaP")
  for X in gammaP:
    if not X in gammaM:
        return X

def getUnset_n(gammaM, gammaP):
  if len(gammaP) == 1:
    print("\nfX == gammaP")
  for n in range(len(gammaP)):
    gammas = list(product(*[gammaP]*n))
    # gammas = ensemble des disjonctions de n modèles de gammaP(de n gammas donc)
    for gamma in gammas:
      # gamma est donc une disjonction de modèles de gammaP (exemple gamma = {1,2} v {1} v {2}). On veut vérifier que
      for X in gamma:
        # X est donc un des modèles de la disjonction qu'est gamma. On va vérifier que tous les 
        if not X in gammaM:
            return X

#Size gammas
def get_Size_Intervalle(gammaM, gammaP):
  size = 0
  n = len(gammaP) - len(gammaM)
  for i in range(n+1):
    size += math.comb(n, i)
  return size

def get_Size_Intervalles(list_gammas):
  size = 0
  for gamma in list_gammas:
    n = len(gamma[1]) - len(gamma[0])
    for i in range(n+1):
      size += math.comb(n, i)
  return size

def get_Cycle_size(agents, acquaintances, beliefs, revision_policies):
  max_gcyc = 0
  mybrg = BRG(agents, acquaintances, beliefs, revision_policies)
  total_it = 1
  bl = [beliefs] # List of belief of the cycle
  rb =  mybrg.revision() # Revised belief

  while(not any([all(rb == bs) for bs in bl])): # bs: belief state
    bl.append(rb)
    rb = mybrg.revision()
    total_it += 1

  max_gcyc = max(max_gcyc, total_it - np.where(np.array([all(rb == bs) for bs in bl]))[0][0])
  return max_gcyc
  # mybrg = BRG(agents, acquaintances, beliefs, revision_policies)

  # Total_it = 1
  # beliefs_list = [beliefs]
  # print("Initial State:")
  # print_beliefs(beliefs)
  # revise_beliefs = mybrg.revision()
  # while(get_index(beliefs_list, revise_beliefs) == -1):
  #   beliefs_list.append(revise_beliefs)
  #   revise_beliefs = mybrg.revision()
  #   Total_it += 1

  # for b in beliefs_list:
  #   print_beliefs(b)
  # cycle_size = Total_it - get_index(beliefs_list, revise_beliefs) 
  # print("Cycle size = ", cycle_size)
    
  # return cycle_size