import cProfile
# To use: cProfile.run('name_function(*args)')
import numpy as np
from algo1_v1 import algo1PA_v1
from algo1_v2 import algo_1_allA_allB, algo_1_allR_allA_allB
from algo2 import algo_2_allR_allA_allB
from RevisionPolicy import RevisionPolicy
from function import generate_acquaintances

if __name__ == "__main__":
  # Alogrithm 1_v1: for all acquaintances and beliefs
  agents = [0, 1, 2]
  pa = generate_acquaintances(agents)
  rp = np.array([RevisionPolicy.init_revision_policy(i) for i in [1, 1, 1]])
  cProfile.run('algo1PA_v1(agents,pa,rp)')

  # Alogrithm 1_v2 variant 1: for all acquaintances and beliefs
  R = [1, 1, 1]
  cProfile.run('algo_1_allA_allB(R, display=True)')

  # Alogrithm 1_v2 variant 2: for all acquaintances, beliefs and policies
  # nbagent = 3;
  # cProfile.run('algo_1_allR_allA_allB(nbagent)')

  # Algorithm 2 : for all beliefs
  nbagent = 3;
  cProfile.run('algo_2_allR_allA_allB(nbagent)')
  pass
