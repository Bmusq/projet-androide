## Stabilité de réseaux dynamiques de croyances

### Description
Ce projet consiste en premier lieu à implémenter de façon efficace et tester un algorithme d'étude de stabilité dans un réseau d’agents évoluant sur le modèle des "jeux de révision de croyance". Les jeux de révision de croyance sont un formalisme pour représenter des mécanismes d’influences entre agents. Chaque agent ayant ces propres croyances (représentées de façon propositionnelle) et un ensemble de relations d’influence, le système évolue tour par tour, à la manière d’une jeu à zero joueur (façon jeu de la vie), chaque agent mettant à jour ses croyances à chaque tour de façon synchrone en prenant en compte les croyances de ses influenceurs (selon des mécanismes tirés des principes de révision de croyances). Cette évolution est forcément cyclique et l’objet d’une étude de stabilité est de déterminer, pour une structure donnée, la taille maximale d’un cycle de croyance (en envisageant toutes les croyances initiales possibles).
L’algorithme fonctionne en projetant ces croyances dans un langage canonique avant d’effectuer un certain nombre de réductions de l’espace des croyances possibles pouvant mener à des cycles différents pour effecteur une recherche exhaustive.

### Code du projet
[Projet sur les jeux de révision de croyance](https://replit.com/@basilemusquer/Projet-Android)

### Articles de recherche sur lesquels se base le projet

[Nicolas Schwind, Katsumi Inoue, Gauvain Bourgne, Sébastien Konieczny, and Pierre Marquis, "Belief Revision game," in 29th AAAI Conference on Artificial Intelligence (AAAI 2015), Texas, USA, January 2015, pp. 1590-1596.](https://www.researchgate.net/publication/272377529_Belief_Revision_Games)

[Schwind Nicolas, Katsumi Inoue, Gauvain Bourgne, Sébastien Konieczny, and Pierre Marquis, "Is Promoting Beliefs Useful to Make Them Accepted in Networks of Agents ?," in 25th International Joint Conference on Artificial Intelligence (IJCAI 2016), New York, USA, July 2016, pp. 1237-1243.](https://www.semanticscholar.org/paper/Is-Promoting-Beliefs-Useful-to-Make-Them-Accepted-Schwind-Inoue/74144e58b1750e5fed487be52601b8b059b4850e)

[Gauvain Bourgne, Yutaro Totsuka, Nicolas Schwind, and Katsumi Inoue, "Identifying Belief Sequences in a Network of Communicating Agents," in 22nd International Conference on Principles and Practice of Multi-Agent Systems (PRIMA 2019), Torino, Italy, October 2019, pp. 370-386.](https://link.springer.com/chapter/10.1007/978-3-030-33792-6_23)

### Rapport du projet 
![Rapport complet](/Rapport_Musquer_Lechapelier.pdf "Image Title")
<p float="left">
  <img src="/RapportPNG/Rapport_Musquer_Lechapelier-01.png" width="300" />
  <img src="/RapportPNG/Rapport_Musquer_Lechapelier-02.png" width="300" /> 
  <img src="/RapportPNG/Rapport_Musquer_Lechapelier-03.png" width="300" />
</p>


